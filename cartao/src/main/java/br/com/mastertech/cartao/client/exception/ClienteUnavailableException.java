package br.com.mastertech.cartao.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cliente indisponível! :c")
public class ClienteUnavailableException extends RuntimeException{
}
