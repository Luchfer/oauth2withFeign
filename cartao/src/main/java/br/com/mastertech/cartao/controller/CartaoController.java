package br.com.mastertech.cartao.controller;


import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.CartaoMapper;
import br.com.mastertech.cartao.model.dto.request.CartaoChangeAtivoRequest;
import br.com.mastertech.cartao.model.dto.request.CreateCartaoRequest;
import br.com.mastertech.cartao.model.dto.response.CartaoAtivoChangedResponse;
import br.com.mastertech.cartao.model.dto.response.CartaoCreatedResponse;
import br.com.mastertech.cartao.model.dto.response.CartaoDetailsResponse;
import br.com.mastertech.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoCreatedResponse create(@Valid @RequestBody CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = cartaoMapper.toCartao(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return cartaoMapper.toCartaoCreatedResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoAtivoChangedResponse changeCartaoAtivo(@PathVariable String numero, @Valid  @RequestBody CartaoChangeAtivoRequest cartaoChangeAtivoRequest) {
        Cartao cartao = cartaoService.changeAtivo(null,numero, cartaoChangeAtivoRequest.getAtivo());
        return cartaoMapper.toCartaoAtivoChangedResponse(cartao);
    }

    @PostMapping("/{idCartao}/expirar")
    public CartaoAtivoChangedResponse expira(@PathVariable long idCartao) {
        Cartao cartao = cartaoService.changeAtivo(idCartao,null, Boolean.FALSE);
        return cartaoMapper.toCartaoAtivoChangedResponse(cartao);
    }

    @GetMapping("/{numero}")
    public CartaoDetailsResponse getByNumero(@PathVariable String numero) {
        Cartao cartao = cartaoService.getByNumero(numero);
        return cartaoMapper.toCartaoDetailsResponse(cartao);
    }

    @GetMapping("id/{id}")
    public CartaoDetailsResponse getById(@PathVariable long id) {
        Cartao cartao = cartaoService.getById(id);
        return cartaoMapper.toCartaoDetailsResponse(cartao);
    }

    @GetMapping("{id}/cliente/{idCliente}")
    public CartaoDetailsResponse getByIdAndCLiente(@PathVariable long idCliente,@PathVariable long id) {
        Cartao cartao = cartaoService.getByIdClienteAndIdCartao(idCliente,id);
        return cartaoMapper.toCartaoDetailsResponse(cartao);
    }

}

