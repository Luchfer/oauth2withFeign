

package br.com.mastertech.cartao.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cartao nao encontrado ou inativo")
public class CartaoNotFoundOrInactiveException extends RuntimeException { }

