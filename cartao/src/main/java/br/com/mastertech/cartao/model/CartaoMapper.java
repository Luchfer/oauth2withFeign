package br.com.mastertech.cartao.model;



import br.com.mastertech.cartao.model.dto.request.CreateCartaoRequest;
import br.com.mastertech.cartao.model.dto.response.CartaoAtivoChangedResponse;
import br.com.mastertech.cartao.model.dto.response.CartaoCreatedResponse;
import br.com.mastertech.cartao.model.dto.response.CartaoDetailsResponse;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(createCartaoRequest.getNumero());
        cartao.setIdCliente(createCartaoRequest.getClienteId());

        return cartao;
    }

    public CartaoCreatedResponse toCartaoCreatedResponse(Cartao cartao) {
        CartaoCreatedResponse cartaoCreatedResponse = new CartaoCreatedResponse();

        cartaoCreatedResponse.setId(cartao.getId());
        cartaoCreatedResponse.setNumero(cartao.getNumero());
        cartaoCreatedResponse.setClienteId(cartao.getIdCliente());
        cartaoCreatedResponse.setAtivo(cartao.getAtivo());

        return cartaoCreatedResponse;
    }

    public CartaoAtivoChangedResponse toCartaoAtivoChangedResponse(Cartao cartao) {
        CartaoAtivoChangedResponse cartaoAtivoChangedResponse = new CartaoAtivoChangedResponse();

        cartaoAtivoChangedResponse.setId(cartao.getId());
        cartaoAtivoChangedResponse.setNumero(cartao.getNumero());
        cartaoAtivoChangedResponse.setClienteId(cartao.getIdCliente());
        cartaoAtivoChangedResponse.setAtivo(cartao.getAtivo());

        return cartaoAtivoChangedResponse;
    }

    public CartaoDetailsResponse toCartaoDetailsResponse(Cartao cartao) {
        CartaoDetailsResponse cartaoDetailsResponse = new CartaoDetailsResponse();

        cartaoDetailsResponse.setId(cartao.getId());
        cartaoDetailsResponse.setNumero(cartao.getNumero());
        cartaoDetailsResponse.setClienteId(cartao.getIdCliente());
        cartaoDetailsResponse.setAtivo(cartao.getAtivo());

        return cartaoDetailsResponse;
    }
}
