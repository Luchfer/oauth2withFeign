package br.com.mastertech.cliente.controller;


import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.model.ClienteMapper;
import br.com.mastertech.cliente.request.ClienteRequest;
import br.com.mastertech.cliente.response.ClienteCreatedResponse;
import br.com.mastertech.cliente.response.ClienteDetailResponse;
import br.com.mastertech.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteMapper clienteMapper;


    @GetMapping("/status")
    @ResponseBody
    public ResponseEntity<String> getStatus() throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body("Service is up");
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteCreatedResponse createCliente(@RequestBody @Valid ClienteRequest clienteRequest, BindingResult result) throws Exception {
        Cliente cliente = clienteMapper.toCliente(clienteRequest);
        cliente=clienteService.createCliente(cliente);
        return clienteMapper.toClienteCreatedResponse(cliente);

    }
    @GetMapping("/{idCliente}")
    @ResponseBody

    public ClienteDetailResponse getCliente(@PathVariable(value = "idCliente") long idCliente){
        Cliente dadosCliente = clienteService.getCliente(idCliente);
        return clienteMapper.toClienteDetailResponse(dadosCliente);
    }
}
